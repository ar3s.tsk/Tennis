package org.nexity.tennis;

import java.util.Optional;

import static org.nexity.tennis.Progression.FOURTY;

public class DeuceGameScore extends GameScore {

    private Optional<Player> advantage = Optional.empty();
    private Optional<Player> winner = Optional.empty();

    @Override
    public void givePointTo(Player playerTag) {
        if(isDeuce()){
            advantage = Optional.of(playerTag);
            return;
        }
        if(playerTag.equals(advantage.get())){
            this.winner = advantage;
            return;
        }
        advantage = Optional.empty();
    }

    @Override
    public String toString() {
        if(winner.isPresent()){
            GameScore finalGameScore = new GameScore(FOURTY, FOURTY);
            finalGameScore.givePointTo(winner.get());
            return finalGameScore.toString();
        }
        if(isDeuce()){
            return "DEUCE";
        }
        return "ADVANTAGE "+advantage.get();
    }

    @Override
    public GameScore getDeuceScore() {
        return this;
    }

    @Override
    public boolean isDeuce() {
        return !advantage.isPresent();
    }
}
