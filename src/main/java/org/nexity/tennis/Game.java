package org.nexity.tennis;

public class Game {

    private GameScore score = new GameScore();

    public void givePointTo(Player playerTag) {
        score.givePointTo(playerTag);
        if(score.isDeuce()){
            score = score.getDeuceScore();
        }
    }

    @Override
    public String toString() {
        return score.toString();
    }
}
