package org.nexity.tennis;

import java.util.EnumMap;
import java.util.Map;

import static org.nexity.tennis.Player.A;
import static org.nexity.tennis.Player.B;
import static org.nexity.tennis.Progression.FOURTY;
import static org.nexity.tennis.Progression.O;

public class GameScore {
    private Map<Player, Progression> progressions = new EnumMap<Player,Progression>(Player.class) {{
        put(A, O);
        put(B, O);
    }};

    public GameScore() {}

    GameScore(Progression aProg, Progression bProg) {
        progressions.put(A,aProg);
        progressions.put(B,bProg);
    }

    public void givePointTo(Player playerTag) {
        Progression progression = progressions.get(playerTag);
        Progression nextStep = progression.getNextStep();
        progressions.put(playerTag, nextStep);
    }

    @Override
    public String toString() {
        Progression aScore = progressions.get(A);
        Progression bScore = progressions.get(B);
        return aScore + " - " + bScore;
    }

    public boolean isDeuce() {
        Progression aScore = progressions.get(A);
        Progression bScore = progressions.get(B);
        return aScore.equals(FOURTY) && bScore.equals(FOURTY);
    }


    public GameScore getDeuceScore() {
        return new DeuceGameScore();
    }
}
