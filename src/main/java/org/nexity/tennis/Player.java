package org.nexity.tennis;

public enum  Player {
    A,B;

    public Player getOpponent() {
        if (this.equals(A)) {
            return B;
        }
        return A;
    }
}
