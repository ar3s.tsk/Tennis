package org.nexity.tennis;

public enum Progression {
    O("0"),FIFTEEN("15"),THIRTY("30"),FOURTY("40"),GAME("Win Game");

    private String displayValue;

    @Override
    public String toString() {
        return displayValue;
    }

    Progression(String displayValue) {
        this.displayValue = displayValue;
    }

    public Progression getNextStep() {
        if(this.equals(GAME)){
            return this;
        }
        return Progression.values()[this.ordinal() + 1];
    }
}
