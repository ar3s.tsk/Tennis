package org.nexity.tennis;

import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;

import static org.nexity.tennis.Player.*;

public class Set {

    private Optional<Player> winner = Optional.empty();
    private Map<Player, Integer> progressions = new EnumMap<Player, Integer>(Player.class) {{
        put(A, 0);
        put(B, 0);
    }};

    @Override
    public String toString() {
        return getPlayerSetScore(A) + " - " + getPlayerSetScore(B);
    }

    private String getPlayerSetScore(Player player) {
        if (winner.isPresent()){
            if (winner.get().equals(player)){
                return "Win Set";
            }
        }
        return progressions.get(player).toString();
    }

    public void giveGameTo(Player player) {
        int newSetScore = progressions.get(player) + 1;

        progressions.put(player, newSetScore);

        if (newSetScore >= 6){
            Integer opponentSetScore = progressions.get(player.getOpponent());
            Integer scoreGap = newSetScore - opponentSetScore;
            if( scoreGap < 2) {
                return;
            }
            winner = Optional.of(player);
        }
    }
}
