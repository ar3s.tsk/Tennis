package org.nexity.tennis;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import static org.assertj.core.api.Assertions.*;
import static org.nexity.tennis.Player.*;

public class Stepdefs {

    private Game sutGame;
    private Set sutSet;

    @Deprecated
    @Given("^I have a step$")
    public void iHaveAStep(){
        System.out.println("I do have a step !");
    }

    @Deprecated
    @When("^I run it$")
    public void iRunIt() {
        System.out.println("The step runs !");
    }

    @Deprecated
    @Then("^it works$")
    public void itWorks() {
        System.out.println("And it \"kinda\" works !");
    }

    @Given("^the game has just started$")
    public void theGameHasJustStarted() {
        sutGame = new Game();
    }

    @Given("^the game is DEUCE$")
    public void the_game_is_DEUCE() {
        theGameHasJustStarted();
        player_X_scores_times(A,3);
        player_X_scores_times(B,3);
    }
    @Given("^the game is \"ADVANTAGE ([AB])\"$")
    public void the_game_is(Player leadingPlayer) {
        the_game_is_DEUCE();
        player_X_scores_the_point(leadingPlayer);
    }

    @When("^player ([AB]) looses the point$")
    public void player_X_looses_the_point(Player player) {
        player_X_scores_the_point(player.getOpponent());
    }

    @When("^player ([AB]) scores the point$")
    public void player_X_scores_the_point(Player player) {
        player_X_scores_times(player, 1);
    }


    @Then("^the score is \"([^\"]*)\"$")
    public void theScoreIs(String scoreString) {
        assertThat(sutGame.toString()).as("checking score").isEqualTo(scoreString);
    }

    @When("^player ([AB]) scores (\\d+) times$")
    public void player_X_scores_times(Player player, int scored) {
        for (int i = 0; i < scored; i++) {
            sutGame.givePointTo(player);
        }
    }

    @Given("^the set has just started$")
    public void theSetHasJustStarted() {
        sutSet = new Set();
    }

    @When("^player ([AB]) scores (\\d+) games?$")
    public void player_X_scores_games(Player player, int gamesScored) {
        for (int i = 0; i < gamesScored; i++) {
            sutSet.giveGameTo(player);
        }
    }

    @And("^both players \\(starting with ([AB])\\) score two consecutive games one after the other repeating this breath-taking cycle (\\d+) times$")
    public void bothPlayersScoreConsecutiveGamesOneAfterTheOtherRepeatingThisBreathTakingCycleTimes(Player player, int cycleAmount) {
        for (int i = 0; i < cycleAmount; i++) {
            player_X_scores_games(player, 2);
            player_X_scores_games(player.getOpponent(), 2);
        }
    }

    @Then("^the set score is \"([^\"]*)\"$")
    public void theSetScoreIs(String setScoreString) {
        assertThat(sutSet.toString()).as("checking set score").isEqualTo(setScoreString);
    }

}