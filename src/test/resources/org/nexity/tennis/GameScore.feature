Feature: Game Scores
  As a tennis referee
  I want to manage the score of a game of a set of a tennis match between 2 players with simple Game rules
  In order to display the current Game score of each player

  In the rest of this spec we will consider that we have two players : A & B
  The acronym SX (X being either A or B) means that the player "X" scored
  A always has his score on the left panel, B always has his on the right

  Scenario: No score
    Given the game has just started
    Then the score is "0 - 0"

  Scenario Outline: If <scoringPlayer> scores <scoring> points the result will be "<endScore>"
    Given the game has just started
    When player <scoringPlayer> scores <scoring> times
    Then the score is "<endScore>"

    Examples:
      | scoringPlayer | scoring | endScore     |
      | A             | 1       | 15 - 0       |
      | A             | 2       | 30 - 0       |
      | A             | 3       | 40 - 0       |
      | A             | 4       | Win Game - 0 |
      | B             | 1       | 0 - 15       |
      | B             | 2       | 0 - 30       |
      | B             | 3       | 0 - 40       |
      | B             | 4       | 0 - Win Game |

  Scenario Outline: Even though <champion> might get a solid start, <outsider> could do a tremendous comeback
    Given the game has just started
    When player <champion> scores 2 times
    And player <outsider> scores 4 times
    Then the score is "<endScore>"

    Examples:
      | champion | outsider | endScore      |
      | A        | B        | 30 - Win Game |
      | B        | A        | Win Game - 30 |

  Scenario: If the 2 players reach the score 40, the DEUCE rule is activated
    Given the game has just started
    When player A scores 3 times
    And player B scores 3 times
    Then the score is "DEUCE"

  Scenario Outline: If the score is DEUCE and <scoringPlayer> wins the point, then <scoringPlayer> takes the ADVANTAGE
    Given the game is DEUCE
    When player <scoringPlayer> scores 1 times
    Then the score is "<endScore>"

    Examples:
      | scoringPlayer | endScore    |
      | A             | ADVANTAGE A |
      | B             | ADVANTAGE B |

  Scenario Outline: If the <advantagePlayer> had the advantage but looses the point, the score is DEUCE again
    Given the game is "ADVANTAGE <advantagePlayer>"
    When player <advantagePlayer> looses the point
    Then the score is "DEUCE"

    Examples:
      | advantagePlayer |
      | A               |
      | B               |

  Scenario Outline: If the <advantagePlayer> had the advantage and scores the point he wins the game
    Given the game is "ADVANTAGE <advantagePlayer>"
    When player <advantagePlayer> scores the point
    Then the score is "<endScore>"

    Examples:
      | advantagePlayer | endScore      |
      | A               | Win Game - 40 |
      | B               | 40 - Win Game |
