Feature: Set Scores
  As a tennis referee
  I want to manage the score of a set of a tennis match between 2 players
  In order to display the current Game & Set score of each player

  In the rest of this spec we will consider that we have two players : A & B
  A always has his score on the left panel, B always has his on the right

  Scenario: No set score
    Given the set has just started
    Then the set score is "0 - 0"

  Scenario Outline: If <scoringPlayer> scores <scoring> games the result will be "<endScore>"
    Given the set has just started
    When player <scoringPlayer> scores <scoring> games
    Then the set score is "<endScore>"

    Examples:
      | scoringPlayer | scoring | endScore    |
      | A             | 1       | 1 - 0       |
      | A             | 2       | 2 - 0       |
      | A             | 3       | 3 - 0       |
      | A             | 4       | 4 - 0       |
      | A             | 5       | 5 - 0       |
      | A             | 6       | Win Set - 0 |
      | B             | 1       | 0 - 1       |
      | B             | 2       | 0 - 2       |
      | B             | 3       | 0 - 3       |
      | B             | 4       | 0 - 4       |
      | B             | 5       | 0 - 5       |
      | B             | 6       | 0 - Win Set |

  Scenario Outline: Even though <champion> might get a VERY solid start, <outsider> could do a ridiculously cinematographic comeback and win
    Given the set has just started
    When player <champion> scores 4 games
    And player <outsider> scores 6 games
    Then the set score is "<endScore>"

    Examples:
      | champion | outsider | endScore    |
      | A        | B        | 4 - Win Set |
      | B        | A        | Win Set - 4 |

  Scenario Outline: Even though <winnerToBe> might win 6 games, <fighter> could force <winnerToBe> to score a 7th to win
    Given the set has just started
    When player <fighter> scores 5 games
    And player <winnerToBe> scores 6 games
    Then the set score is "<intermediateScore>"
    When player <winnerToBe> scores 1 game
    Then the set score is "<endScore>"

    Examples:
      | winnerToBe | fighter | intermediateScore | endScore    |
      | A          | B       | 6 - 5             | Win Set - 5 |
      | B          | A       | 5 - 6             | 5 - Win Set |

  Scenario Outline: Even though <sadFighter> might be ahead 6 games to 5, <winnerToBe> could win if he scores a 6th, 7th and an 8th game
    Given the set has just started
    When player <winnerToBe> scores 5 games
    And player <sadFighter> scores 6 games
    Then the set score is "<intermediateScore>"
    When player <winnerToBe> scores 2 games
    Then the set score is "<comebackScore>"
    When player <winnerToBe> scores 1 game
    Then the set score is "<gameSetAndMatchScore>"

    Examples:
      | winnerToBe | sadFighter | intermediateScore | comebackScore | gameSetAndMatchScore |
      | A          | B          | 5 - 6             | 7 - 6         | Win Set - 6          |
      | B          | A          | 6 - 5             | 6 - 7         | 6 - Win Set          |

  Scenario Outline: (BONUS) Tie-Breaks could be VERY long ...
    Given the set has just started
    When player <kingOfTennis> scores 5 games
    And player <godOfTennis> scores 6 games
    And both players (starting with <kingOfTennis>) score two consecutive games one after the other repeating this breath-taking cycle 53 times
    Then the set score is "<lastCliffhangerScore>"
    When player <godOfTennis> scores 1 game
    Then the set score is "<gameSetAndMatchScore>"

    Examples:
      | godOfTennis | kingOfTennis | lastCliffhangerScore | gameSetAndMatchScore |
      | A           | B            | 112 - 111            | Win Set - 111        |
      | B           | A            | 111 - 112            | 111 - Win Set        |
